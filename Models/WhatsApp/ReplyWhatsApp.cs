public class Context
    {
        public string message_id { get; set; }
    }

    public class ReplyWhatsApp
    {
        public string messaging_product { get; set; }
        public string recipient_type { get; set; }
        public string to { get; set; }
        public Context context { get; set; }
        public string type { get; set; }
        public ReplyText text { get; set; }
    }

    public class ReplyText
    {
        public bool preview_url { get; set; }
        public string body { get; set; }
    }