    public class SendContact
    {
        public string input { get; set; }
        public string wa_id { get; set; }
    }

    public class SendMessage
    {
        public string id { get; set; }
    }

    public class SendWhatsAppResponse
    {
        public string messaging_product { get; set; }
        public List<Contact> contacts { get; set; }
        public List<Message> messages { get; set; }
    }
