// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Action
    {
        public List<Button> buttons { get; set; }
    }

    public class Body
    {
        public string text { get; set; }
    }

    public class Button
    {
        public string type { get; set; }
        public Reply reply { get; set; }
    }

    public class InteractiveBtn
    {
        public string type { get; set; }
        public Body body { get; set; }
        public Action action { get; set; }
    }

    public class Reply
    {
        public string id { get; set; }
        public string title { get; set; }
    }

    public class InteractiveButton
    {
        public string messaging_product { get; set; }
        public string recipient_type { get; set; }
        public string to { get; set; }
        public string type { get; set; }
        public InteractiveBtn interactive { get; set; }
    }

