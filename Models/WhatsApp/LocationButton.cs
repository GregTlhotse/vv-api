    public class LocationAction
    {
        public string name { get; set; }
    }

    public class LocationBody
    {
        public string text { get; set; }
    }

    public class LocationInteractive
    {
        public string type { get; set; }
        public LocationBody body { get; set; }
        public LocationAction action { get; set; }
    }

    public class Location
    {
        public string messaging_product { get; set; }
        public string recipient_type { get; set; }
        public string type { get; set; }
        public string to { get; set; }
        public LocationInteractive interactive { get; set; }
    }
	public class LocationResponse
{
    public double longitude { get; set; }
    public double latitude { get; set; }
    public string name { get; set; }
    public string address { get; set; }
}