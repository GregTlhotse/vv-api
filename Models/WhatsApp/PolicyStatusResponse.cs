public class PolicyStatusResponse
    {
        public string PolicyId { get; set; }
        public string PolicyNumber { get; set; }
        public string Status { get; set; }
        public string ClientIdName { get; set; }
    }