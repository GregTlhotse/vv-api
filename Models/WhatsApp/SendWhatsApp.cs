 
    public class SendWhatsApp
    {
        public string messaging_product { get; set; }
        public string recipient_type { get; set; }
        public string to { get; set; }
        public string type { get; set; }
        public SendText text { get; set; }
        public Image image { get; set; }
    }

    public class SendText
    {
        public bool preview_url { get; set; }
        public string body { get; set; }
    }
     public class Image
    {
        public string link { get; set; }
        public string caption { get; set; }
    }