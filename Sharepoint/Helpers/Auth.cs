 public class Auth
    {
       
        public string Grant_type { get; set; }
        public string Client_id { get; set; }
        public string Client_secret { get; set; }
        public string Scope { get; set; }
        public string Tenant_id { get; set; }
    }
    public class AuthRespose
    {
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public int ext_expires_in { get; set; }
        public string access_token { get; set; }
    }