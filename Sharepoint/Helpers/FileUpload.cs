 public class FileUpload
    {
        public List<FileInfo> File { get; set; }

        public string FolderPath { get; set; }
        
        public SharepointConnector Connector { get; set; }
    }

    public class FileInfo{
        public string FileContent { get; set; }
        public string Id { get; set; }
        public string FileName { get; set; }
        public string Entity { get; set; }
    }
    public class DeleteFile{
        public string Id { get; set; }
        public SharepointConnector Connector { get; set; }
    }
        public class FetchFile{

        public string Id { get; set; }
        public SharepointConnector Connector { get; set; }
    }
    public class UploadResponse{
        public List<UploadedFile> UploadedFiles { get; set; }
        public bool IsSuccess { get; set; }
        public UploadResponse()
        {
            UploadedFiles = new List<UploadedFile>();
        }
    }
    public class UploadedFile{
        public string Id { get; set; }
        public string WebUrl { get; set; }
        public string RequestId {get;set;}
    }
public class DeleteFileResponse{
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
}
public class SharepointConnector{
     public string SiteName { get; set; }
  public string ClientId { get; set; }
  public string ClientSecret { get; set; }
  public string TenantId { get; set; }
  public string Scope { get; set; }
 
}