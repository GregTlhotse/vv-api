using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System.Text;
using Azure.Identity;
using Microsoft.Graph;
using Microsoft.Graph.Drives.Item.Items.Item.CreateLink;
using Microsoft.Graph.Drives.Item.Items.Item.CreateUploadSession;
using Microsoft.Graph.Models;
using System.Data.SqlClient;
using Dapper;


namespace ezra360.Proxy.SharepointService
{
    public class SharepointService : ISharepointService
    {
        readonly IConfiguration _configuration;
        public SharepointService(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task<UploadResponse> ExecuteUpload(FileUpload fileUpload)
        {


            var scopes = new[] { fileUpload.Connector.Scope };
            //"https://microsoft.sharepoint-df.com/Sites.FullControl.All","https://microsoft.sharepoint-df.com/User.ReadWrite.All","https://graph.microsoft.com/.default"
            // Values from app registration
            // var clientId = "ffb92dbf-ab8f-4195-8346-b48f1f0b5103";
            // var tenantId = "3e50aa65-2819-4fa2-890c-6556e29623f5";
            // var clientSecret = "r498Q~nKNsnR-4hi.A5nQXv7oa-oiItBXbKHAcgk";
            //Site name "Ezra-360" DriveType "documentLibrary"
            UploadResponse uploadResponse = new UploadResponse();
            // using Azure.Identity;
            var options = new ClientSecretCredentialOptions
            {
                AuthorityHost = AzureAuthorityHosts.AzurePublicCloud,
            };
            var clientSecretCredential = new ClientSecretCredential(fileUpload.Connector.TenantId, fileUpload.Connector.ClientId, fileUpload.Connector.ClientSecret, options);
            try
            {
                var graphClient = new GraphServiceClient(clientSecretCredential, scopes);
                var sites = await graphClient.Sites.GetAsync();
                var site = sites.Value.FirstOrDefault(x => x.Name == fileUpload.Connector.SiteName);
                var drives = await graphClient.Sites[site?.Id].Drives.GetAsync();
                var drive = drives.Value.FirstOrDefault(x => x.DriveType == "documentLibrary");

                foreach (var doc in fileUpload.File)
                {//Ezra-Documents/UAT/NSFAS/Facility
                    var stream = new MemoryStream(Convert.FromBase64String(doc.FileContent));
                    var session = await graphClient.Drives[drive?.Id].Root.ItemWithPath($"{fileUpload.FolderPath}/{doc.Entity}/{doc.Id}/{doc.FileName}")
                        .CreateUploadSession.PostAsync(new CreateUploadSessionPostRequestBody
                        {
                            AdditionalData = new Dictionary<string, object>
                            {
                                { "@microsoft.graph.conflictBehavior", "replace" }
                            }
                        });
                    var fileTask = new LargeFileUploadTask<DriveItem>(session, stream);
                    var result = await fileTask.UploadAsync();
                    var p = await graphClient.Drives[drive?.Id].Items[result.ItemResponse.Id].CreateLink.PostAsync(new CreateLinkPostRequestBody());
                    var res = await graphClient.Drives[drive?.Id].Root.ItemWithPath($"{fileUpload.FolderPath}/{doc.Entity}/{doc.Id}/{doc.FileName}").GetAsync();
                    // Replace Base64 with URL from sharePoint res.WebUrl
                    
                    uploadResponse.UploadedFiles.Add(new UploadedFile
                    {
                        RequestId = doc.Id,
                        Id = res.Id,
                    });

                }
                uploadResponse.IsSuccess = true;
                Console.WriteLine("Done");

                return uploadResponse;

            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
                uploadResponse.IsSuccess = false;
                return uploadResponse;
            }
        }
        public async Task<DeleteFileResponse> DeleteFile(DeleteFile deleteFile)
        {
            var scopes = new[] { deleteFile.Connector.Scope };
            //"https://microsoft.sharepoint-df.com/Sites.FullControl.All","https://microsoft.sharepoint-df.com/User.ReadWrite.All","https://graph.microsoft.com/.default"
            // Values from app registration
            // var clientId = "ffb92dbf-ab8f-4195-8346-b48f1f0b5103";
            // var tenantId = "3e50aa65-2819-4fa2-890c-6556e29623f5";
            // var clientSecret = "r498Q~nKNsnR-4hi.A5nQXv7oa-oiItBXbKHAcgk";
            //Site name "Ezra-360" DriveType "documentLibrary"

            // using Azure.Identity;
            var options = new ClientSecretCredentialOptions
            {
                AuthorityHost = AzureAuthorityHosts.AzurePublicCloud,
            };
            var clientSecretCredential = new ClientSecretCredential(deleteFile.Connector.TenantId, deleteFile.Connector.ClientId, deleteFile.Connector.ClientSecret, options);
            try
            {
                var graphClient = new GraphServiceClient(clientSecretCredential, scopes);
                var sites = await graphClient.Sites.GetAsync();
                var site = sites.Value.FirstOrDefault(x => x.Name == deleteFile.Connector.SiteName);
                var drives = await graphClient.Sites[site?.Id].Drives.GetAsync();
                var drive = drives.Value.FirstOrDefault(x => x.DriveType == "documentLibrary");
                // var res = await graphClient.Drives[drive?.Id].Root.ItemWithPath($"{deleteFile.FolderPath}/{deleteFile.Entity}/{deleteFile.Id}/{deleteFile.FileName}").GetAsync();
                // await graphClient.Drives[drive?.Id].Items[res.Id].DeleteAsync();
                await graphClient.Drives[drive?.Id].Items[deleteFile.Id].DeleteAsync();

                Console.WriteLine("Done delete");
                 
                return new DeleteFileResponse {
                    IsSuccess = true,
                    Message = "Deleted file"
                };

            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
                return new DeleteFileResponse {
                    IsSuccess = false,
                    Message = "Failed to delete file"
                };
            }

        }
        public async Task<DriveItem> DriveItem(FetchFile fetchFile){
          var scopes = new[] { fetchFile.Connector.Scope };

            var options = new ClientSecretCredentialOptions
            {
                AuthorityHost = AzureAuthorityHosts.AzurePublicCloud,
            };
            var clientSecretCredential = new ClientSecretCredential(fetchFile.Connector.TenantId, fetchFile.Connector.ClientId, fetchFile.Connector.ClientSecret, options);
            try
            {
                var graphClient = new GraphServiceClient(clientSecretCredential, scopes);
                var sites = await graphClient.Sites.GetAsync();
                var site = sites.Value.FirstOrDefault(x => x.Name == fetchFile.Connector.SiteName);
                var drives = await graphClient.Sites[site?.Id].Drives.GetAsync();
                var drive = drives.Value.FirstOrDefault(x => x.DriveType == "documentLibrary");
                return await graphClient.Drives[drive?.Id].Items[fetchFile.Id].GetAsync();

            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
                return null;
            }

        }

    }
}