using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ezra360.Proxy.SharePoint.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/sharepoint")]
    public class SharePointController : ControllerBase
    {
        readonly ISharepointService _service;
        public SharePointController(ISharepointService sharepointService){
            _service = sharepointService;
        }
        /// <summary>
        /// This API end-point is used to upload ezra documents to SharePoint using base64 format.
        /// </summary>
        [HttpPost("upload")]
        public async Task<IActionResult> ExcuteUpload([FromBody] FileUpload file)
        {
            return Ok(await _service.ExecuteUpload(file));

        }
        /// <summary>
        /// This API end-point is used to delete ezra document from SharePoint.
        /// </summary>
        [HttpPost("delete")]
        public async Task<IActionResult> ExcuteDelete([FromBody] DeleteFile file)
        {
            return Ok(await _service.DeleteFile(file));
        }
        /// <summary>
        /// This API end-point is used to get ezra document from SharePoint.
        /// </summary>
        [HttpPost("fetch-file")]
        public async Task<IActionResult> ExcuteGet([FromBody] FetchFile file)
        {
            return Ok(await _service.DriveItem(file));
        }
    }
}