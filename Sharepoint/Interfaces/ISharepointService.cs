using Microsoft.Graph.Models;

public interface ISharepointService{
    Task<UploadResponse> ExecuteUpload(FileUpload fileUpload);
    Task<DeleteFileResponse> DeleteFile(DeleteFile deleteFile);
     Task<DriveItem> DriveItem(FetchFile fetchFile);
}