using System.Diagnostics;
using System.Globalization;
using System.Text;
using NIWS_NIF;
using NIWS_Partner;
using NIWS_Validation;

namespace ezra360.Proxy.NetCashService
{
    public class NetCashService : INetCashService
    {
        NIWS_NIFClient NIFClient = new NIWS_NIFClient();
        NIWS_PartnerClient NIWS_PartnerClient = new NIWS_PartnerClient();
        NIWS_ValidationClient NIWS_ValidationClient = new NIWS_ValidationClient();
        readonly IConfiguration _configuration;
        readonly IWebHostEnvironment _env;
        string salaryKey = String.Empty;
        string accountKey = String.Empty;

        public NetCashService(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            // accountKey = _configuration.GetValue<string>("NetCashSettings:AccountServiceKey");
            // salaryKey = _configuration.GetValue<string>("NetCashSettings:SalaryServiceKey");


        }
        public async Task<string> BatchFileUploadAsync(BatchFileViewModel data)
        {
            try
            {
                // Check Banking Details
                for (var i = 0; i < data.Accounts.Count; i++)
                {
                    var validate = await ValidateBankAccount(data.Connector.AccountServiceKey, data.Accounts[i].BankAccountNumber, data.Accounts[i].BranchCode, data.Accounts[i].BankAccountType);
                    switch (validate)
                    {
                        case 1:
                            return "Invalid branch code " + data.Accounts[i].BranchCode + " for account " + data.Accounts[i].BankAccountNumber;
                        case 2:
                            return "Account number " + data.Accounts[i].BankAccountNumber + " failed check digit validation";
                        case 3:
                            return "Invalid account type for " + data.Accounts[i].BankAccountNumber;
                        case 4:
                            return "Input data incorrect";
                        case 100:
                            return "Authentication failed while checking bank details";
                        case 200:
                            return "Web service error contact support@netcash.co.za";
                    }
                }
                if (ProcessFile(data) == String.Empty) { return "Could not proccess the file!"; };
                return await ProcessBatchFileUploadAsync(data);

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string ProcessFile(BatchFileViewModel data)
        {
            var header = FileStringFormatter.GetStringHeader()
                        .Replace("(KEY)", data.Connector.SalaryServiceKey)
                        .Replace("(Instruction)", data.Instruction)
                        .Replace("(ActionDate)", data.ActionDate.Date.ToString("yyyyMMdd"));

            try
            {
                // IFormatProvider culture = new CultureInfo("en-US", true); 
                // DateTime dateVal = DateTime.ParseExact(data.ActionDate.ToString(), "yyyy-MM-dd", culture);
                var sb = new StringBuilder();
                if (data.Accounts.Count > 0)
                {
                    double totalAmt = 0;
                    for (var i = 0; i < data.Accounts.Count; i++)
                    {
                        totalAmt += Convert.ToDouble(data.Accounts[i].Amount);
                        sb.Append(FileStringFormatter.GetStringBody()
                                    .Replace("(Account Reference)", data.Accounts[i].AccountReference + DateTime.Now.Year + i)
                                    .Replace("(Account Name)", data.Accounts[i].AccountName)
                                    .Replace("(Bank Detail Type)", data.Accounts[i].BankDetailType)
                                    .Replace("(Bank Account)", data.Accounts[i].BankAccount)
                                    .Replace("(Bank account type)", data.Accounts[i].BankAccountType)
                                    .Replace("(Branch code)", data.Accounts[i].BranchCode))
                                    .Replace("(Filler)", data.Accounts[i].Filler)
                                    .Replace("(Bank account number)", data.Accounts[i].BankAccountNumber)
                                    .Replace("(Amount)", data.Accounts[i].Amount)
                                    // .Replace("(Default Amount)", data.Accounts[i].DefaultAmount) // Used for Update
                                    // .Replace("(Default Beneficiary statement reference)", data.Accounts[i].DefaultBeneficiaryStatementReference)
                                    .Replace("(Beneficiary statement reference)", data.Accounts[i].BeneficiaryStatementReference);
                        sb.AppendLine();
                    }
                    var footer = FileStringFormatter.GetStringFooter().Replace("(Total Transactions)", data.Accounts.Count.ToString()).Replace("(Total Amounts)", totalAmt.ToString());
                    if (_env.IsDevelopment())
                    {
                        File.WriteAllText(Path.GetFullPath(FileStringFormatter.FolderPath), header + sb.ToString() + footer);
                    }
                    return header + sb.ToString() + footer;
                }

            }
            catch (Exception ex)
            {
                return String.Empty;
            }
            return String.Empty;


        }
        public async Task<int> ValidateBankAccount(string serviceKey, string accNum, string brachCode, string accountType)
        {
            var check = await NIWS_ValidationClient.ValidateBankAccountAsync(serviceKey, accNum, brachCode, accountType);
            return Convert.ToInt16(check);
        }
        public async Task<string> Validate(string serviceKey, string accNum, string brachCode, string accountType)
        {
            var validate = await ValidateBankAccount(serviceKey, accNum, brachCode, accountType);
            switch (validate)
            {
                case 0:
                    return "Bank account details valid";
                case 1:
                    return "Invalid branch code " + brachCode + " for account " + accNum;
                case 2:
                    return "Account number " + accNum + " failed check digit validation";
                case 3:
                    return "Invalid account type for " + accNum;
                case 4:
                    return "Input data incorrect";
                case 100:
                    return "Authentication failed while checking bank details";
                default:
                    return "Web service error contact support@netcash.co.za";


            }
        }
        public async Task<string> ProcessBatchFileUploadAsync(BatchFileViewModel data)
        {
            var fl = ProcessFile(data);
            var prosecessFile = await NIFClient.BatchFileUploadAsync(data.Connector.SalaryServiceKey, fl);
            // string Request = await NIFClient.RequestFileUploadReportAsync(salaryKey, prosecessFile);
            switch (prosecessFile)
            {
                case "100":  // Authentication failure
                    prosecessFile = "Authentication failure";
                    break;
                case "101":  // Date format error
                    prosecessFile = "Date format error. If the string contains a date, it should be in the format CCYYMMDD";
                    break;
                case "102":  // Parameter error
                    prosecessFile = "Parameter error. One or more of the parameters in the string is incorrect.";
                    break;
                case "200":  // General code exception.
                    prosecessFile = "General code exception";
                    break;
                default:  // successful
                    return prosecessFile;
            }
            //  await NIFClient.CloseAsync();
            return prosecessFile;
        }
        public async Task<string> RequestMerchantStatement(MerchantStatementViewModel merchantStatementViewModel)
        {

            string Request = await NIFClient.RequestMerchantStatementAsync(merchantStatementViewModel.Connector.AccountServiceKey, merchantStatementViewModel.FromActionDate.ToString("yyyyMMdd"));
            
            switch (Request)

            {
                case "100":  // Authentication failure
                   return "Authentication failure";

                case "101":  // Date format error.CCYYMMDD
                    return "Date format error.CCYYMMDD";

                case "102":  // Invalid date
                        return "Invalid date";

                case "200":  // General code exception
                        return "General code exception";

                default:  // Success, use file token to call RetrieveMerchantStatement
                    return await NIFClient.RetrieveMerchantStatementAsync(merchantStatementViewModel.Connector.AccountServiceKey,Request);
                    
            }
            

        }

    }
}
