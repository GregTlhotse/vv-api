using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ezra360.Proxy.NetCash.Controllers
{
[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/netcash")]
public class NetcashController : ControllerBase
{
    readonly INetCashService _netCashService;
    public NetcashController(INetCashService netCashService){
      _netCashService = netCashService;
    }
/// <summary>
/// This API end-point is used to make batch payments.
/// </summary>
/// <remarks>
/// Instruction values can be either:
///
///     "Update – update the master file without loading a batch", "PaySalaries  – Sameday salary batch upload", "DatedSalaries - Dated salary batch upload", "Realtime  – Sameday creditor batch upload", "DatedPayments – Dated creditor batch upload"
/// </remarks>
    [HttpPost("batch-payment")]
    public async Task<IActionResult> BatchFile(BatchFileViewModel batchFileViewModel){
        return Ok(await _netCashService.BatchFileUploadAsync(batchFileViewModel));
    }
/// <summary>
/// This API end-point validate bank account.
/// </summary>
    [HttpPost("validate-bank")]
    public async Task<IActionResult> ValidateBank(ValidateBankViewModel validateBankViewModel){
        return Ok(await _netCashService.Validate(validateBankViewModel.Connector.AccountServiceKey,validateBankViewModel.BankAccountNumber,validateBankViewModel.BranchCode,validateBankViewModel.BankAccountType));
    }
    /// <summary>
/// This API end-point is used to request a statement. Date should not be current or future date.
/// </summary>
    [HttpPost("statement")]
    public async Task<IActionResult> RequestMerchantStatement(MerchantStatementViewModel merchantStatementViewModel){
        return Ok(await _netCashService.RequestMerchantStatement(merchantStatementViewModel));
    }

}
}