public static class FileStringFormatter{
    public static string FolderPath = "NetCash\\NetFile\\Netcash.txt";
    public static string GetStringHeader(){
        string fl = "H\t(KEY)\t1\t(Instruction)\tXiquelTestBatch\t(ActionDate)\t24ade73c-98cf-47b3-99be-cc7b867b3080" +
            Environment.NewLine + "K\t101\t102\t131\t132\t133\t134\t135\t136\t162\t252" + Environment.NewLine;
        return fl;
    }
    public static string GetStringBody(){
        string fl = "T\t(Account Reference)"  + "\t(Account Name)"  + "\t(Bank Detail Type)"  +
                    "\t(Bank Account)"  + "\t(Bank account type)" + "\t(Branch code)" +
                    "\t(Filler)"  + "\t(Bank account number)" + 
                    "\t(Amount)" + "\t(Beneficiary statement reference)";
        return fl;
    }
    public static string GetStringFooter(){
        string fl = "F\t(Total Transactions)\t(Total Amounts)\t9999";
        return fl;
    }
}