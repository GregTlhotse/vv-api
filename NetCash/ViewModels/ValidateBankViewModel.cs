

public class ValidateBankViewModel{
    public string BranchCode { get; set; }
    public string BankAccountNumber { get; set; }
    public string BankAccountType { get; set; }
    public NetConnector Connector { get; set; }
}