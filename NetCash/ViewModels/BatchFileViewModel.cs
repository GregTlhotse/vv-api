
public class BatchFileViewModel{
    public string Instruction { get; set; }
    // public string ServiceKey { get; set; }
    public DateTime ActionDate { get; set; }
    public List<ContentViewModel> Accounts { get; set; }
    public NetConnector Connector { get; set; }
}
public class NetConnector{
    public string PayNowServiceKey { get; set; }
    public string SalaryServiceKey { get; set; }
    public string AccountServiceKey { get; set; }
}