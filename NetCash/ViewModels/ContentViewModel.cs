public class ContentViewModel{
    public string AccountReference { get; set; }
    public string AccountName { get; set; }
    public string BankDetailType { get; set; }
    public string BankAccount { get; set; }
    public string BankAccountType { get; set; }
    public string BranchCode { get; set; }
    public string Filler { get; set; }
    public string BankAccountNumber { get; set; }
    public string Amount { get; set; }
    
    public string BeneficiaryStatementReference { get; set; }
}