public interface INetCashService{
     Task<string> BatchFileUploadAsync(BatchFileViewModel data);
     Task<string> Validate(string serviceKey,string accNum, string brachCode, string accountType);
     Task<string> RequestMerchantStatement(MerchantStatementViewModel merchantStatementViewModel);
}