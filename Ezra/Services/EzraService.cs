using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using ezra360.Proxy.Interfaces;

namespace ezra360.Proxy.EzraService{
    public class EzraService:IEzraService{
        readonly IConfiguration _configuration;
        readonly RestClient client;
         public EzraService(IConfiguration configuration){
            _configuration = configuration;
            var options = new RestClientOptions(_configuration.GetValue<string>("EzraSettings:URL"))
            {
                MaxTimeout = -1,
            };
             client = new RestClient(options, configureSerialization: s => s.UseNewtonsoftJson());
            
        }
        public async Task<List<PolicyStatusResponse>>GetPolicyStatus(string policyNumber){
            List<PolicyStatusResponse> policyStatusResponse = null;

            var request = new RestRequest("api/v1/Entities/Data/GetPolicyStatus?PolicyNumber="+policyNumber, Method.Get);
            request.AddHeader("api-key", _configuration.GetValue<string>("EzraSettings:API-KEY"));
            RestResponse response = await client.ExecuteAsync(request);
             Console.WriteLine(response.Content);
            if(response.IsSuccessful){
                policyStatusResponse = JsonConvert.DeserializeObject<List<PolicyStatusResponse>>(response.Content);
            }
           
            return policyStatusResponse;
        }
        public async Task<List<BranchResponse>>GetBranches(){
            List<BranchResponse> policyStatusResponse = null;

            var request = new RestRequest("api/v1/Entities/Data/GetBranches", Method.Get);
            request.AddHeader("api-key", _configuration.GetValue<string>("EzraSettings:API-KEY"));
            RestResponse response = await client.ExecuteAsync(request);
             Console.WriteLine(response.Content);
            if(response.IsSuccessful){
                policyStatusResponse = JsonConvert.DeserializeObject<List<BranchResponse>>(response.Content);
            }
           
            return policyStatusResponse;
        }
        public async Task<PickupResponse> CreatePickup(Pickup pickup){
            PickupResponse pickupResponse = null;
            var request = new RestRequest("api/v1.0/entities/ExecuteRequest", Method.Post);
            request.AddHeader("api-key", _configuration.GetValue<string>("EzraSettings:API-KEY"));
            request.AddStringBody(JsonConvert.SerializeObject(pickup), DataFormat.Json);
            RestResponse response = await client.ExecuteAsync(request);
             Console.WriteLine(response.Content);
            if(response.IsSuccessful){
                pickupResponse = JsonConvert.DeserializeObject<PickupResponse>(response.Content);
            }
            return pickupResponse;
        }
        public async Task<PickupResponse> CreateDeseased(Deseased deseased){
            PickupResponse pickupResponse = null;
            var request = new RestRequest("api/v1.0/entities/ExecuteRequest", Method.Post);
            request.AddHeader("api-key", _configuration.GetValue<string>("EzraSettings:API-KEY"));
            request.AddStringBody(JsonConvert.SerializeObject(deseased), DataFormat.Json);
            RestResponse response = await client.ExecuteAsync(request);
             Console.WriteLine(response.Content);
            if(response.IsSuccessful){
                pickupResponse = JsonConvert.DeserializeObject<PickupResponse>(response.Content);
            }
            return pickupResponse;
        }
        public string Login(LoginViewModel user)
        {
            if (user.Username == "Greg" && user.Password == "2024") // To be removed by Ezra Login Endpoint
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
                var Sectoken = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                  _configuration["Jwt:Issuer"],
                  null,
                  expires: DateTime.Now.AddMinutes(120),
                  signingCredentials: credentials);
                var token = new JwtSecurityTokenHandler().WriteToken(Sectoken);
                return token;
            }
            return "Failed to login!";
        }


    }
}