
using Microsoft.AspNetCore.Mvc;
using ezra360.Proxy.Interfaces;

namespace ezra360.Proxy.Ezra.Controllers
{
    [Route("api/v{version:apiVersion}/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
      readonly IEzraService _service;
        public UserController(IEzraService service)
        {
            _service = service;
        }
        /// <summary>
        /// This API end-point is used to login and get token to be authorized to use the end-points.
        /// </summary>
        [HttpPost("login")]
        
        public IActionResult Login([FromBody] LoginViewModel user)
        {
            return Ok(new {token = _service.Login(user)});
        }
    }
}