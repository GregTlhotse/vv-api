namespace ezra360.Proxy.Interfaces;
public interface IEzraService{
    Task<List<PolicyStatusResponse>>GetPolicyStatus(string policyNumber);
     string Login(LoginViewModel user);
     Task<List<BranchResponse>>GetBranches();
     Task<PickupResponse> CreatePickup(Pickup pickup);
     Task<PickupResponse> CreateDeseased(Deseased deseased);

}