// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Entity
    {
        public string SourceChannel { get; set; }
        public string PickUpStatus { get; set; }
        public string PlaceOfPickUp { get; set; }
        public string PickUpAddress { get; set; }
        public string Branch { get; set; }
        public string DueDate { get; set; }
        public string Informant1FullName { get; set; }
        public string Informant1Contact { get; set; }
         public string Longitude { get; set; }
        public string Latitude { get; set; }
    }

    public class InputParamters
    {
        public Entity Entity { get; set; }
    }

    public class Pickup
    {
        public string entityName { get; set; }
        public string requestName { get; set; }
        public InputParamters inputParamters { get; set; }

    }

    public class DeseasedEntity
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string PlaceOfDeath { get; set; }
        public string PickUpId { get; set; }
    }

    public class DeseasedInputParamters
    {
        public DeseasedEntity Entity { get; set; }
    }

    public class Deseased
    {
        public string entityName { get; set; }
        public string requestName { get; set; }
        public DeseasedInputParamters inputParamters { get; set; }
    }

