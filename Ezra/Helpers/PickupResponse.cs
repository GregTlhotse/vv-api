// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class OutputParameters
    {
        public UpsertResponse UpsertResponse { get; set; }
    }

    public class PickupResponse
    {
        public bool isSuccess { get; set; }
        public object results { get; set; }
        public object gotoUrl { get; set; }
        public object clientExecuteFunctionOnSuccess { get; set; }
        public object clientExecuteFunctionOnFailure { get; set; }
        public string clientMessage { get; set; }
        public OutputParameters outputParameters { get; set; }
    }

    public class UpsertResponse
    {
        public string recordId { get; set; }
        public object record { get; set; }
        public bool success { get; set; }
        public object errorMesssage { get; set; }
        public int upsertAction { get; set; }
    }

