﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NIWS_Partner
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ValidateServiceKeyRequest", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS")]
    public partial class ValidateServiceKeyRequest : object
    {
        
        private string MerchantAccountField;
        
        private NIWS_Partner.ServiceInfoList ServiceInfoListField;
        
        private string SoftwareVendorKeyField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MerchantAccount
        {
            get
            {
                return this.MerchantAccountField;
            }
            set
            {
                this.MerchantAccountField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public NIWS_Partner.ServiceInfoList ServiceInfoList
        {
            get
            {
                return this.ServiceInfoListField;
            }
            set
            {
                this.ServiceInfoListField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SoftwareVendorKey
        {
            get
            {
                return this.SoftwareVendorKeyField;
            }
            set
            {
                this.SoftwareVendorKeyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ServiceInfoList", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS", ItemName="ServiceInfo")]
    public class ServiceInfoList : System.Collections.Generic.List<NIWS_Partner.ServiceInfo>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ServiceInfo", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS")]
    public partial class ServiceInfo : object
    {
        
        private string ServiceIdField;
        
        private string ServiceKeyField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ServiceId
        {
            get
            {
                return this.ServiceIdField;
            }
            set
            {
                this.ServiceIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ServiceKey
        {
            get
            {
                return this.ServiceKeyField;
            }
            set
            {
                this.ServiceKeyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ValidateServiceKeyResponse", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS")]
    public partial class ValidateServiceKeyResponse : object
    {
        
        private string AccountStatusField;
        
        private string MerchantAccountField;
        
        private NIWS_Partner.ServiceInfoResponseList ServiceInfoField;
        
        private string SoftwareVendorKeyField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AccountStatus
        {
            get
            {
                return this.AccountStatusField;
            }
            set
            {
                this.AccountStatusField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MerchantAccount
        {
            get
            {
                return this.MerchantAccountField;
            }
            set
            {
                this.MerchantAccountField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public NIWS_Partner.ServiceInfoResponseList ServiceInfo
        {
            get
            {
                return this.ServiceInfoField;
            }
            set
            {
                this.ServiceInfoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SoftwareVendorKey
        {
            get
            {
                return this.SoftwareVendorKeyField;
            }
            set
            {
                this.SoftwareVendorKeyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ServiceInfoResponseList", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS", ItemName="ServiceInfoResponse")]
    public class ServiceInfoResponseList : System.Collections.Generic.List<NIWS_Partner.ServiceInfoResponse>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ServiceInfoResponse", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS")]
    public partial class ServiceInfoResponse : object
    {
        
        private string ServiceIdField;
        
        private string ServiceKeyField;
        
        private string ServiceStatusField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ServiceId
        {
            get
            {
                return this.ServiceIdField;
            }
            set
            {
                this.ServiceIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ServiceKey
        {
            get
            {
                return this.ServiceKeyField;
            }
            set
            {
                this.ServiceKeyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ServiceStatus
        {
            get
            {
                return this.ServiceStatusField;
            }
            set
            {
                this.ServiceStatusField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ValidateComplianceRequest", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS")]
    public partial class ValidateComplianceRequest : object
    {
        
        private string MerchantAccountField;
        
        private string SoftwareVendorKeyField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MerchantAccount
        {
            get
            {
                return this.MerchantAccountField;
            }
            set
            {
                this.MerchantAccountField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SoftwareVendorKey
        {
            get
            {
                return this.SoftwareVendorKeyField;
            }
            set
            {
                this.SoftwareVendorKeyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ValidateComplianceResponse", Namespace="http://schemas.datacontract.org/2004/07/NC.DG.TMS.C.WCF.NIWS")]
    public partial class ValidateComplianceResponse : object
    {
        
        private string AccountStatusField;
        
        private string MerchantAccountField;
        
        private string SoftwareVendorKeyField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AccountStatus
        {
            get
            {
                return this.AccountStatusField;
            }
            set
            {
                this.AccountStatusField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MerchantAccount
        {
            get
            {
                return this.MerchantAccountField;
            }
            set
            {
                this.MerchantAccountField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SoftwareVendorKey
        {
            get
            {
                return this.SoftwareVendorKeyField;
            }
            set
            {
                this.SoftwareVendorKeyField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="NIWS_Partner.INIWS_Partner")]
    public interface INIWS_Partner
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/BatchFileUpload", ReplyAction="http://tempuri.org/INIWS_Partner/BatchFileUploadResponse")]
        System.Threading.Tasks.Task<string> BatchFileUploadAsync(string serviceKey, string file);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/RequestFileUploadReport", ReplyAction="http://tempuri.org/INIWS_Partner/RequestFileUploadReportResponse")]
        System.Threading.Tasks.Task<string> RequestFileUploadReportAsync(string serviceKey, string fileToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/ValidateServiceKey", ReplyAction="http://tempuri.org/INIWS_Partner/ValidateServiceKeyResponse")]
        System.Threading.Tasks.Task<NIWS_Partner.ValidateServiceKeyResponse> ValidateServiceKeyAsync(NIWS_Partner.ValidateServiceKeyRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/AccountSupportMessage", ReplyAction="http://tempuri.org/INIWS_Partner/AccountSupportMessageResponse")]
        System.Threading.Tasks.Task<string> AccountSupportMessageAsync(string partnerServiceKey, string serviceKey);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/RequestAccountStatus", ReplyAction="http://tempuri.org/INIWS_Partner/RequestAccountStatusResponse")]
        System.Threading.Tasks.Task<string> RequestAccountStatusAsync(string partnerServiceKey, string pastelAccountNo);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/RetrieveAccountStatus", ReplyAction="http://tempuri.org/INIWS_Partner/RetrieveAccountStatusResponse")]
        System.Threading.Tasks.Task<string> RetrieveAccountStatusAsync(string ServiceKey, string FileToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/RetrievePNSHandle", ReplyAction="http://tempuri.org/INIWS_Partner/RetrievePNSHandleResponse")]
        System.Threading.Tasks.Task<string> RetrievePNSHandleAsync(string ServiceKey, int SystemUserID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/PartnerAuth", ReplyAction="http://tempuri.org/INIWS_Partner/PartnerAuthResponse")]
        System.Threading.Tasks.Task<string> PartnerAuthAsync(string ServiceKey, int SystemUserID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/CreateOrUpdatePNSHandle", ReplyAction="http://tempuri.org/INIWS_Partner/CreateOrUpdatePNSHandleResponse")]
        System.Threading.Tasks.Task<string> CreateOrUpdatePNSHandleAsync(string ServiceKey, int SystemUserID, string PNSHandle);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/DeletePNSHandle", ReplyAction="http://tempuri.org/INIWS_Partner/DeletePNSHandleResponse")]
        System.Threading.Tasks.Task<string> DeletePNSHandleAsync(string ServiceKey, int SystemUserID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INIWS_Partner/ValidateComplianceStatus", ReplyAction="http://tempuri.org/INIWS_Partner/ValidateComplianceStatusResponse")]
        System.Threading.Tasks.Task<NIWS_Partner.ValidateComplianceResponse> ValidateComplianceStatusAsync(NIWS_Partner.ValidateComplianceRequest request);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    public interface INIWS_PartnerChannel : NIWS_Partner.INIWS_Partner, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    public partial class NIWS_PartnerClient : System.ServiceModel.ClientBase<NIWS_Partner.INIWS_Partner>, NIWS_Partner.INIWS_Partner
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public NIWS_PartnerClient() : 
                base(NIWS_PartnerClient.GetDefaultBinding(), NIWS_PartnerClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.WSHttpBinding_INIWS_Partner.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public NIWS_PartnerClient(EndpointConfiguration endpointConfiguration) : 
                base(NIWS_PartnerClient.GetBindingForEndpoint(endpointConfiguration), NIWS_PartnerClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public NIWS_PartnerClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(NIWS_PartnerClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public NIWS_PartnerClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(NIWS_PartnerClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public NIWS_PartnerClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<string> BatchFileUploadAsync(string serviceKey, string file)
        {
            return base.Channel.BatchFileUploadAsync(serviceKey, file);
        }
        
        public System.Threading.Tasks.Task<string> RequestFileUploadReportAsync(string serviceKey, string fileToken)
        {
            return base.Channel.RequestFileUploadReportAsync(serviceKey, fileToken);
        }
        
        public System.Threading.Tasks.Task<NIWS_Partner.ValidateServiceKeyResponse> ValidateServiceKeyAsync(NIWS_Partner.ValidateServiceKeyRequest request)
        {
            return base.Channel.ValidateServiceKeyAsync(request);
        }
        
        public System.Threading.Tasks.Task<string> AccountSupportMessageAsync(string partnerServiceKey, string serviceKey)
        {
            return base.Channel.AccountSupportMessageAsync(partnerServiceKey, serviceKey);
        }
        
        public System.Threading.Tasks.Task<string> RequestAccountStatusAsync(string partnerServiceKey, string pastelAccountNo)
        {
            return base.Channel.RequestAccountStatusAsync(partnerServiceKey, pastelAccountNo);
        }
        
        public System.Threading.Tasks.Task<string> RetrieveAccountStatusAsync(string ServiceKey, string FileToken)
        {
            return base.Channel.RetrieveAccountStatusAsync(ServiceKey, FileToken);
        }
        
        public System.Threading.Tasks.Task<string> RetrievePNSHandleAsync(string ServiceKey, int SystemUserID)
        {
            return base.Channel.RetrievePNSHandleAsync(ServiceKey, SystemUserID);
        }
        
        public System.Threading.Tasks.Task<string> PartnerAuthAsync(string ServiceKey, int SystemUserID)
        {
            return base.Channel.PartnerAuthAsync(ServiceKey, SystemUserID);
        }
        
        public System.Threading.Tasks.Task<string> CreateOrUpdatePNSHandleAsync(string ServiceKey, int SystemUserID, string PNSHandle)
        {
            return base.Channel.CreateOrUpdatePNSHandleAsync(ServiceKey, SystemUserID, PNSHandle);
        }
        
        public System.Threading.Tasks.Task<string> DeletePNSHandleAsync(string ServiceKey, int SystemUserID)
        {
            return base.Channel.DeletePNSHandleAsync(ServiceKey, SystemUserID);
        }
        
        public System.Threading.Tasks.Task<NIWS_Partner.ValidateComplianceResponse> ValidateComplianceStatusAsync(NIWS_Partner.ValidateComplianceRequest request)
        {
            return base.Channel.ValidateComplianceStatusAsync(request);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.WSHttpBinding_INIWS_Partner))
            {
                System.ServiceModel.WSHttpBinding result = new System.ServiceModel.WSHttpBinding();
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                result.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                result.Security.Transport.ClientCredentialType = System.ServiceModel.HttpClientCredentialType.None;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.WSHttpBinding_INIWS_Partner))
            {
                return new System.ServiceModel.EndpointAddress("https://ws.netcash.co.za/NIWS/NIWS_Partner.svc");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return NIWS_PartnerClient.GetBindingForEndpoint(EndpointConfiguration.WSHttpBinding_INIWS_Partner);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return NIWS_PartnerClient.GetEndpointAddress(EndpointConfiguration.WSHttpBinding_INIWS_Partner);
        }
        
        public enum EndpointConfiguration
        {
            
            WSHttpBinding_INIWS_Partner,
        }
    }
}
