public class ReplyViewModel
{
    public string SenderPhoneNumber { get; set; }
    public string SenderId { get; set; }
    public string IncomingText { get; set; }
}