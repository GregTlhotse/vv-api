public class InteractiveButtonViewModel
{
    public string SenderPhoneNumber { get; set; }
    public string SenderId { get; set; }
    public string Button_Title { get; set; }
    public string Button_1 { get; set; }
    public string Button_2 { get; set; }
}