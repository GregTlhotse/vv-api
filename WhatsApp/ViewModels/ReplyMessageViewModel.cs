public class ReplyMessageViewModel
{
    public string SenderPhoneNumber { get; set; }
    public string SenderId { get; set; }
    public string IncomingText { get; set; }
    public string MessageId { get; set; }
}