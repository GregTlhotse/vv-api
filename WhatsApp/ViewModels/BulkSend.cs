using CsvHelper.Configuration;

public class BulkSend
{
    public List<ContactInfo> Contacts { get; set; }
    public string SenderId { get; set; }
    public string Message { get; set; }
}
public class ContactInfo
{
    public string Name { get; set; }
    public string ContactNumber { get; set; }
}
public class CSVBulkSend
{
    public string SenderId { get; set; }
    public string Message { get; set; }

    public IFormFile File { get; set; }
}
public class CSVContact
{
    public string FirstName { get; set; }
    public string Mobile { get; set; }
 

}
public class CSVContactMap : ClassMap<CSVContact>
{
    public CSVContactMap()
    {
        Map(m => m.FirstName).Index(0);
        Map(m => m.Mobile).Index(4);

    }
}
