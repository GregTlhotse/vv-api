using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ezra360.Proxy.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace ezra360.Proxy.WhatsApp.Controllers
{
    [Route("api/v{version:apiVersion}/whatsapp")]
    [ApiController]
    public class WhatsAppController : ControllerBase
    {
        readonly IWhatsAppService _service;
        readonly ILogger<WhatsAppController> _logger;

        public WhatsAppController(IWhatsAppService service,ILogger<WhatsAppController> logger)
        {
            _service = service;
            _logger = logger;
        }
        /// <summary>
        /// This API end-point is used to connect and monitor requests.
        /// </summary>                                                                                  
        [Route("webhook")]
        [HttpPost, HttpGet]
        public async Task<IActionResult> PostAsyc()
        {

            var req = Request;
            try
            {
                string mode = req.Query["hub.mode"];
                string challenge = req.Query["hub.challenge"];
                string verifyToken = req.Query["hub.verify_token"];
                if (!String.IsNullOrEmpty(mode) && !String.IsNullOrEmpty(challenge))
                {
                    if (mode == "subscribe" && verifyToken == "Hello Greg")
                    {
                        string responseMessage = challenge;
                        return new OkObjectResult(responseMessage);
                    }
                    else
                    {
                        return new BadRequestObjectResult("Error");
                    }
                }
                else
                {
                    string requesBody = await new StreamReader(Request.Body).ReadToEndAsync();

                    var response = JsonConvert.DeserializeObject<WhatsAppResponse>(requesBody);
                     
                    if (response.entry[0].changes[0].value.messages.Count > 0)
                    {
                        Console.WriteLine("Message Id: " + response.entry[0].changes[0].value.messages[0].id);
                        Console.WriteLine("Name: " + response.entry[0].changes[0].value.contacts[0].profile.name);
                        long time = Convert.ToInt64(response.entry[0].changes[0].value.messages[0].timestamp);
                        DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(time);
                        DateTime dateTimeSent = dateTimeOffset.DateTime;
                        DateTime currentTimestamp = DateTime.Now;

                        TimeSpan difference = currentTimestamp - dateTimeSent.AddHours(2);
                        // Check if the difference is less than 12 minutes
                         Console.WriteLine("Total Minutes: " + difference.TotalMinutes);
                        if (difference.TotalMinutes < 12)
                        {
                            MessageManager.SetPhoneNumber(response.entry[0].changes[0].value.messages[0].from);
                            await _service.WhatsAppAction(response);
                        }

                    }

                    return Ok();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.LogInformation(ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
        /// <summary>
        /// This API end-point is used to reply to conversation.
        /// </summary> 
        [HttpPost("reply")]
        public async Task<IActionResult> PostReplyAsyc([FromBody] ReplyViewModel replyViewModel)
        {
            try
            {
                await _service.WhatsAppReply(replyViewModel);
                return Ok();

            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
        /// <summary>
        /// This API end-point is used to send same message to multiple contacts.
        /// </summary> 
        [HttpPost("bulk/send")]
        public async Task<IActionResult> PostBulkSendAsyc([FromBody] BulkSend bulk)
        {
            try
            {
                await _service.WhatsAppBulkMessage(bulk);
                return Ok();

            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
        /// <summary>
        /// This API end-point is used to retrieve phone numbers sending messages.
        /// </summary> 
        [HttpGet("phone/numbers/sending/messages")]
        public IActionResult ListPhoneSendingMessage()
        {
            try
            {
                return Ok(MessageManager.GetPhoneNumbers());

            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
        /// <summary>
        /// This API end-point is used to reply to a specific message using message id.
        /// </summary>
        [HttpPost("by-message-id")]
        public async Task<IActionResult> PostReplyMessageAsyc([FromBody] ReplyMessageViewModel replyMessageViewModel)
        {
            try
            {
                await _service.WhatsAppReplyMessage(replyMessageViewModel);
                return Ok();

            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
        /// <summary>
        /// This API end-point is used to reply to a converstion using image and a caption message.
        /// </summary>
        [HttpPost("caption/image")]
        public async Task<IActionResult> PostReplyWithimageAsyc([FromBody] ReplyViewModel replyViewModel)
        {
            try
            {
                await _service.WhatsAppWelcomeWithImage(replyViewModel);
                return Ok();
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
        /// <summary>
        /// This API end-point is used to reply to a converstion using button(s).
        /// </summary>
        [HttpPost("yesOrNo/button")]
        public async Task<IActionResult> PostReplyWithYesNoAsyc([FromBody] InteractiveButtonViewModel replyViewModel)
        {
            try
            {
                await _service.WhatsAppInteractiveButtons(replyViewModel);
                return Ok();
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
        [HttpPost("send-via-excel-file")]
        public ActionResult UploadFile([FromForm] CSVBulkSend cSVBulkSend)
        {
            return Ok(_service.ReadCSV<dynamic>(cSVBulkSend));
        }


    }
}
