namespace ezra360.Proxy.Interfaces;
public interface IMessageAPIService{
    Task SendWhatsApp(SendWhatsApp sendWhatsApp, string senderId);
    Task GetDocument(string media_id);
    Task MarkAsRead(string id, string senderId);
    Task SendMessageReplyWhatsApp(ReplyWhatsApp sendWhatsApp, string senderId);
    Task SendinteractiveWhatsApp(InteractiveButton sendWhatsApp, string senderId);
    Task SendLocationWhatsApp(Location sendWhatsApp, string senderId);
    Task SendMenuTemplateWhatsApp(MenuTemplate sendWhatsApp, string senderId);
}