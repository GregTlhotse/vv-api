using RestSharp;

namespace ezra360.Proxy.Interfaces;
public interface IWhatsAppService{
    Task WhatsAppAction(WhatsAppResponse whatsAppResponse);
    Task WhatsAppReply(ReplyViewModel replyViewModel);
    Task WhatsAppReplyMessage(ReplyMessageViewModel replyMessageViewModel);
     Task WhatsAppWelcomeWithImage(ReplyViewModel replyViewModel);
     Task WhatsAppInteractiveButtons(InteractiveButtonViewModel replyViewModel);
     Task WhatsAppBulkMessage(BulkSend bulkSend);
     Task WhatsAppCSVBulkMessage(CSVBulkSend bulkSend);
     public IEnumerable<T> ReadCSV<T>(CSVBulkSend cSVBulkSend);
}