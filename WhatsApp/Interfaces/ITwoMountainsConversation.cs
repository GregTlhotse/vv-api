using RestSharp;

namespace ezra360.Proxy.Interfaces;
public interface ITwoMountainsConversation{
    Task TwoMountainsConversationsAction(WhatsAppResponse whatsAppResponse);
    Task WhatsAppReply(ReplyViewModel replyViewModel);
    Task WhatsAppReplyMessage(ReplyMessageViewModel replyMessageViewModel);
     Task WhatsAppWelcomeWithImage(ReplyViewModel replyViewModel);
     Task WhatsAppInteractiveButtons(InteractiveButtonViewModel replyViewModel);
}