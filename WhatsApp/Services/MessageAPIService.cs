using System.Net.Http.Headers;
using Newtonsoft.Json;
using ezra360.Proxy.Interfaces;

namespace ezra360.Proxy.MessageAPIService
{
    public class MessageAPIService : IMessageAPIService
    {
        readonly IConfiguration _configuration;
        readonly ILogger<MessageAPIService> _logger;

        public MessageAPIService(IConfiguration configuration,ILogger<MessageAPIService> logger)
        {
            _configuration = configuration;
             _logger = logger;

        }
        public async Task MarkAsRead(string id, string senderId)
        {
            string APIUrl = _configuration.GetValue<string>("WhatsAppSettings:APIUrl") + senderId + "/messages";
            MarkAsReadViewModel markAsReadViewModel = new MarkAsReadViewModel
            {
                message_id = id,
                messaging_product = "whatsapp",
                status = "read"

            };
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.GetValue<string>("WhatsAppSettings:Token"));
                var content = JsonContent.Create(markAsReadViewModel);
                var response = await client.PostAsync(APIUrl, content);
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                    Console.WriteLine("Seen message successfully");
                    // string result = await response.Content.ReadAsStringAsync();
                    // var receivedMessage = JsonConvert.DeserializeObject<SendWhatsAppResponse>(result);

                }
                else
                {
                    Console.WriteLine("Failed to open message");
                }
            }
        }
        public async Task SendWhatsApp(SendWhatsApp sendWhatsApp, string senderId)
        {
            
            string APIUrl = _configuration.GetValue<string>("WhatsAppSettings:APIUrl") + senderId + "/messages";
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.GetValue<string>("WhatsAppSettings:Token"));
                var content = JsonContent.Create(sendWhatsApp);
                var response = await client.PostAsync(APIUrl, content);
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                    Console.WriteLine("Sent successfully");

                    // string result = await response.Content.ReadAsStringAsync();
                    // var receivedMessage = JsonConvert.DeserializeObject<SendWhatsAppResponse>(result);

                }
                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    Console.WriteLine("Failed to send");
                }
            };
        }
        public async Task SendinteractiveWhatsApp(InteractiveButton sendWhatsApp, string senderId)
        {
            string APIUrl = _configuration.GetValue<string>("WhatsAppSettings:APIUrl") + senderId + "/messages";
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.GetValue<string>("WhatsAppSettings:Token"));
                var content = JsonContent.Create(sendWhatsApp);
                var response = await client.PostAsync(APIUrl, content);
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                    Console.WriteLine("Sent successfully");

                    // string result = await response.Content.ReadAsStringAsync();
                    // var receivedMessage = JsonConvert.DeserializeObject<SendWhatsAppResponse>(result);

                }
                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    Console.WriteLine("Failed to send:" + await response.Content.ReadAsStringAsync());
                }
            };
        }
        public async Task GetDocument(string media_id)
        {
            string APIUrl = _configuration.GetValue<string>("WhatsAppSettings:APIUrl") + media_id;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.GetValue<string>("WhatsAppSettings:Token"));
                var response = await client.GetAsync(APIUrl);
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                    string result = await response.Content.ReadAsStringAsync();
                    var receivedDocument = JsonConvert.DeserializeObject<DocumentResponse>(result);
                    client.DefaultRequestHeaders.UserAgent.ParseAdd(_configuration.GetValue<string>("WhatsAppSettings:AgentHeader"));
                    var getDocBytes = await client.GetByteArrayAsync(receivedDocument.url);
                }
                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    Console.WriteLine("Failed to get document");
                }
            };
        }
        public async Task SendMessageReplyWhatsApp(ReplyWhatsApp sendWhatsApp, string senderId)
        {
            string APIUrl = _configuration.GetValue<string>("WhatsAppSettings:APIUrl") + senderId + "/messages";
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.GetValue<string>("WhatsAppSettings:Token"));
                var content = JsonContent.Create(sendWhatsApp);
                var response = await client.PostAsync(APIUrl, content);
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                    Console.WriteLine("Sent successfully");
                    // string result = await response.Content.ReadAsStringAsync();
                    // var receivedMessage = JsonConvert.DeserializeObject<SendWhatsAppResponse>(result);

                }
                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    Console.WriteLine("Failed to send");
                }
            };
        }
        public async Task SendLocationWhatsApp(Location sendWhatsApp, string senderId)
        {
            string APIUrl = _configuration.GetValue<string>("WhatsAppSettings:APIUrl") + senderId + "/messages";
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.GetValue<string>("WhatsAppSettings:Token"));
                var content = JsonContent.Create(sendWhatsApp);
                var response = await client.PostAsync(APIUrl, content);
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                    Console.WriteLine("Sent successfully");

                    // string result = await response.Content.ReadAsStringAsync();
                    // var receivedMessage = JsonConvert.DeserializeObject<SendWhatsAppResponse>(result);

                }
                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    Console.WriteLine("Failed to send");
                }
            };
        }
        public async Task SendMenuTemplateWhatsApp(MenuTemplate sendWhatsApp, string senderId)
        {
            string APIUrl = _configuration.GetValue<string>("WhatsAppSettings:APIUrl") + senderId + "/messages";
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.GetValue<string>("WhatsAppSettings:Token"));
                var content = JsonContent.Create(sendWhatsApp);
                var response = await client.PostAsync(APIUrl, content);
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                    Console.WriteLine("Sent successfully");

                    // string result = await response.Content.ReadAsStringAsync();
                    // var receivedMessage = JsonConvert.DeserializeObject<SendWhatsAppResponse>(result);

                }
                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    Console.WriteLine("Failed to send");
                }
            };
        }
    }
}