using Newtonsoft.Json;
using RestSharp;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using ezra360.Proxy.Interfaces;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;

namespace ezra360.Proxy.WhatsAppService
{
    public class WhatsAppService : IWhatsAppService
    {
        readonly IConfiguration _configuration;
        readonly IMessageAPIService _messageApi;
        readonly ITwoMountainsConversation _twoMountainsConversation;
        static List<MessageBody> messageBodies = new List<MessageBody>();


        public WhatsAppService(IConfiguration configuration, IMessageAPIService api, ITwoMountainsConversation twoMountainsConversation, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _messageApi = api;
            _twoMountainsConversation = twoMountainsConversation;
            messageBodies = MessageManager.BodyList;
        }
        public async Task WhatsAppAction(WhatsAppResponse response)
        {
            //Based on the sender Id then send from the Customer WhatsApp Number using if statement to cater for more bussiness numbers added on the meta deverloper console
            if (response.entry[0].changes[0].value.metadata.phone_number_id == _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"))
            {
                await _twoMountainsConversation.TwoMountainsConversationsAction(response);
            }
            //   await _twoMountainsConversation.TwoMountainsConversationsAction(response);
        }
        public async Task WhatsAppReply(ReplyViewModel replyViewModel)
        {
            var sendWhatsApp = new SendWhatsApp
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "text",
                recipient_type = "individual",
                text = new SendText { preview_url = true, body = replyViewModel.IncomingText }

            };
            await _messageApi.SendWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppReplyMessage(ReplyMessageViewModel replyMessageViewModel)
        {
            var sendReplyWhatsApp = new ReplyWhatsApp
            {
                messaging_product = "whatsapp",
                to = replyMessageViewModel.SenderPhoneNumber,
                type = "text",
                recipient_type = "individual",
                text = new ReplyText { preview_url = true, body = replyMessageViewModel.IncomingText },
                context = new Context { message_id = replyMessageViewModel.MessageId }

            };
            await _messageApi.SendMessageReplyWhatsApp(sendReplyWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppBulkMessage(BulkSend bulkSend)
        {
            var sendWhatsApp = new SendWhatsApp
            {
                messaging_product = "whatsapp",
                to = "",
                type = "text",
                recipient_type = "individual",
                text = new SendText { preview_url = true, body = "" }

            };
            for (var i = 0; i < bulkSend.Contacts.Count; i++)
            {
                sendWhatsApp.to = bulkSend.Contacts[i].ContactNumber;
                sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterBulkText").Message.Replace("[name]", bulkSend.Contacts[i].Name).Replace("[message]", bulkSend.Message) };
                await _messageApi.SendWhatsApp(sendWhatsApp, bulkSend.SenderId);
            }


        }
        public async Task WhatsAppWelcomeWithImage(ReplyViewModel replyViewModel)
        {
            var sendWhatsApp = new SendWhatsApp
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "image",
                recipient_type = "individual",
                image = new Image { link = BodyText.IMAGE, caption = replyViewModel.IncomingText }

            };
            await _messageApi.SendWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppInteractiveButtons(InteractiveButtonViewModel replyViewModel)
        {
            var btn1 = new Button
            {
                type = "reply",
                reply = new Reply { id = "1", title = replyViewModel.Button_1 }
            };
            var btn2 = new Button
            {
                type = "reply",
                reply = new Reply { id = "2", title = replyViewModel.Button_2 }
            };
            var list = new List<Button>
            {
                btn1,
                btn2
            };

            var sendWhatsApp = new InteractiveButton
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "interactive",
                recipient_type = "individual",
                interactive = new InteractiveBtn { type = "button", body = new Body { text = replyViewModel.Button_Title }, action = new Action { buttons = list } }

            };
            await _messageApi.SendinteractiveWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppBranchButtons(InteractiveButtonViewModel replyViewModel)
        {
            var btn1 = new Button
            {
                type = "reply",
                reply = new Reply { id = "1", title = replyViewModel.Button_1 }
            };
            var btn2 = new Button
            {
                type = "reply",
                reply = new Reply { id = "2", title = replyViewModel.Button_2 }
            };
            var list = new List<Button>
            {
                btn1,
                btn2
            };

            var sendWhatsApp = new InteractiveButton
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "interactive",
                recipient_type = "individual",
                interactive = new InteractiveBtn { type = "button", body = new Body { text = replyViewModel.Button_Title }, action = new Action { buttons = list } }

            };
            await _messageApi.SendinteractiveWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppCSVBulkMessage(CSVBulkSend bulkSend)
        {
            var sendWhatsApp = new SendWhatsApp
            {
                messaging_product = "whatsapp",
                to = "",
                type = "text",
                recipient_type = "individual",
                text = new SendText { preview_url = true, body = "" }

            };
            try
            {
                var badPeople = new List<string>();
                using var memoryStream = new MemoryStream(new byte[bulkSend.File.Length]);
                await bulkSend.File.CopyToAsync(memoryStream);
                memoryStream.Position = 0;
                var config = new CsvConfiguration(CultureInfo.CurrentCulture)
                {
                    Delimiter = ",",
                    HasHeaderRecord = true,
                    TrimOptions = TrimOptions.Trim,
                    MissingFieldFound = null,
                    BadDataFound = arg => badPeople.Add(arg.Context.Parser.RawRecord),
                    Encoding = Encoding.UTF8,
                    HeaderValidated = null
                };

                using (var reader = new StreamReader(memoryStream))
                using (var csv = new CsvReader(reader, config))
                {
                    var records = csv.GetRecords<CSVContact>().ToList();
                    for (var i = 0; i < records.Count; i++)
                    {
                        sendWhatsApp.to = "27" + records[i].Mobile.Substring(1);
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterBulkText").Message.Replace("[name]", records[i].FirstName).Replace("[message]", bulkSend.Message) };
                        await _messageApi.SendWhatsApp(sendWhatsApp, bulkSend.SenderId);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }



        }
        public IEnumerable<T> ReadCSV<T>(CSVBulkSend cSVBulkSend)
        {
            var reader = new StreamReader(cSVBulkSend.File.OpenReadStream());
            var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csv.GetRecords<T>();
            return records;
        }

    }


}