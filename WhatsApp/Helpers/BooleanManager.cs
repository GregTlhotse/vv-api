public static class BooleanManager
{
    public static bool isClaim = false;
    public static bool isHowToClaim = false;
    public static bool isUpdateContact = false;
    public static bool lookForDetails = false;
    public static bool foundDetails = false;
    public static bool communicateFinance = false;
    public static bool claimNotFound = false;
    public static bool isWelcome = true;
    public static bool isPendingDocument = false;
    public static bool isDocumentUploaded = false;

    public static bool isEarlyDays = false;
    public static bool isMissingContact = false;
    public static bool isCorrectContactSecurity = false; //check security
    public static bool isConfirmedContactSecurity = false; //check security

    public static String policyNumber = "";
    public static bool isPolicyStatus = false;
    public static bool isPickup = false;
    public static bool isPickingBranch = false;
    public static bool isPickingPickup = false;
    public static bool isDeceasedFirstName = false;
    public static bool isDeceasedSurname { get; set; }
    public static bool isDeceasedDone { get; set; }
    public static bool isDeceasedPlaceOfDeath { get; set; }


    public static void Reset(){
      isClaim = false;
    isHowToClaim = false;
    isUpdateContact = false;
    lookForDetails = false;
    foundDetails = false;
    communicateFinance = false;
    claimNotFound = false;
    isWelcome = true;
    isPendingDocument = false;
    isDocumentUploaded = false;
    isEarlyDays = false;
    isMissingContact = false;
    isCorrectContactSecurity = false;
    isConfirmedContactSecurity = false;
    isPolicyStatus = false;
    isPickup = false;
    isPickingBranch = false;
    isDeceasedFirstName = false;
    isDeceasedSurname = false;
    isDeceasedDone = false;
    isDeceasedPlaceOfDeath = false;
    }
    
}