public static class MessageManager
{
    public static readonly List<MessageBody> BodyList = new List<MessageBody>(){
    new MessageBody(){
        Message = "Please provide me with your policy number so I can assist you further.",Identifier = "EnterClaimText",isTrue = false

    },
    new MessageBody(){
        Message = "I'm sorry to hear that. It's possible there may be an issue with the contact details we have on file. To assist you better, can you please provide me with your ID number?",Identifier = "EnterMissingContactText",isTrue = false

    },
    new MessageBody(){
        Message = "Let's start with your \uD83D\uDCCD pickup. You can either *manually enter an address* or *share your location.*..",Identifier = "EnterLocationText",isTrue = false

    },
    new MessageBody(){
        Message = "https://www.twomountains.co.za/wp-content/uploads/2022/05/Two-Mountains-New-Logo.png",Identifier = "IMAGE",isTrue = false

    },
    new MessageBody(){
        Message = "Thank you for providing your policy number. Please wait a moment while I verify your claim status.",Identifier = "EnterThankForPolicy",isTrue = false

    },
    new MessageBody(){
        Message = "I found your claim. It has been approved but not yet paid. I understand the importance of this, and I'll help you through the process. To ensure your payment is processed, I'll need to add it to our upcoming payment batch.\n\n\nMay I know if you have any specific details you'd like to be added to the batch?",Identifier = "EnterFoundClaimText",isTrue = false

    },
    new MessageBody(){
        Message = "The status shows that there are some missing documents causing the delay. You can easily submit the missing documents here. Would you like to proceed with that?",Identifier = "EnterStatusShowMissingDocumentsText",isTrue = false

    },
    new MessageBody(){
        Message = "Typically, we require basic information like your full name, contact number, and bank details for the transaction. However, if there's any additional or specific information you think we should know, please mention it.",Identifier = "EnterTypicallyRequireText",isTrue = false

    },
    new MessageBody(){
        Message = "Thank you for the information. I have added your details to our upcoming payment batch. Our finance team will process it, and I'll let you know once it's uploaded.\n\nCan you wait for a moment while I communicate with our finance team?",Identifier = "EnterUpcomingPaymentText",isTrue = false

    },
    new MessageBody(){
        Message = "Thank you for your patience. I've communicated with our finance department, and your payment has been added to batch number [batchNumber]. You can expect your payment to be processed by [payDay].\n\nIf there's any delay or if you don't receive the payment by then, please contact our customer service at 0800 668 539.",Identifier = "EnterFinanceDepCom",isTrue = false

    }
    ,
    new MessageBody(){
        Message = "Choose your nearest branch please: ",Identifier = "EnterNearestBranchText",isTrue = false

    }
    ,
    new MessageBody(){
        Message = "Your pickup has been created succesfully. Thank you \ud83d\ude0a",Identifier = "CreatedPickupText",isTrue = false

    },
    new MessageBody(){
        Message = "Please enter the *deceased first name*",Identifier = "EnterDeceasedNameText",isTrue = false

    },
    new MessageBody(){
        Message = "Please enter the *deceased surname*",Identifier = "EnterDeceasedSurnameText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "Please enter the *deceased place of death*",Identifier = "EnterDeceasedPlaceOfDeathText",isTrue = false
    }
     ,
    new MessageBody(){
        Message = "You're welcome! If you have any other questions or need further assistance in the future, please don't hesitate to reach out. Have a great day! \ud83d\ude0a",Identifier = "EnterThankYouText",isTrue = false
    },
    new MessageBody(){
        Message = "Thank you for the information provided \uD83D\uDE4F. ",Identifier = "EnterDeceasedThankYouText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "Sorry something went wrong while receiving information provided.",Identifier = "EnterSorryText",isTrue = false
    },
    new MessageBody(){
        Message = "Great! Please upload the necessary documents here.",Identifier = "EnterGoAheadUpoladText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "You need to upload the necessary documents here.",Identifier = "EnterYouNeedToUploadText",isTrue = false
    }
     ,
    new MessageBody(){
        Message = "Thank you for submitting the documents. I have forwarded them for verification. Once they're verified, your claim process will continue. You will receive updates on the progress via SMS. Please ensure your contact details are up to date with us...",Identifier = "EnterThankYouSubmittedUploadText",isTrue = false
    },
    new MessageBody(){
        Message = "Your claim was recently submitted, and it generally takes up to 14 days to determine the result of a cash claim. You're still within that time frame. We appreciate your patience. You'll receive SMS updates as we move through the process.",Identifier = "EnterCashClaimText",isTrue = false
    },
    new MessageBody(){
        Message = "I found the contact details linked to your policy. For security reasons, I can't display them here, but I can assist you in updating them if needed. However, before we proceed, we need to verify that I'm communicating with the correct policyholder.\n\nPlease provide me with your date of birth and the answer to your security question: [Security Question, e.g., What's your mother's maiden name?].",Identifier = "EnterFoundContactText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "Thank you for verifying your identity. Please provide the correct contact number where you wish to receive the SMS updates.",Identifier = "EnterVerifyContact",isTrue = false
    }
        ,
    new MessageBody(){
        Message = "Thank you. I have updated your contact number to [incomingText]. Moving forward, you should receive all SMS updates on this number.\n\nIf you face any issues or if there's any delay in receiving updates, please contact our customer service at 0800 668 539.",Identifier = "EnterConfirmedSecurityText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "I'm sorry, I'm unable to fetch the status of your claim at the moment. Please contact our customer service at 0800 668 539 for further assistance. My apologies for any inconvenience.",Identifier = "EnterClaimNotFoundText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "Hi [name]\n\n Your status is [status]\n\n Thank you.",Identifier = "EnterPolicyStatusText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "Your policy was not found! Please verify your policy number.\n\n\n9️⃣9️⃣) Exit",Identifier = "EnterPolicyStatusNotFoundText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "*Pick the branch close to you using the number given*:\n\n",Identifier = "EnterBranchesTitleText",isTrue = false
    }
    ,
    new MessageBody(){
        Message = "Pick place of the pickup:\n\n",Identifier = "EnterPlaceOfPickupTitleText",isTrue = false
    },
    new MessageBody(){
        Message = "Hi [name]\n\n[message]",Identifier = "EnterBulkText",isTrue = false
    }
};
    public static void SetTrueAtIndex(List<MessageBody> bodyList, string identifier)
    {
        // Set the element at the specified index to true
        bodyList.Where(x => x.Identifier == identifier).FirstOrDefault().isTrue = true;

        // Set all other elements to false
        for (int i = 0; i < bodyList.Count; i++)
        {
            if (bodyList[i].Identifier != identifier)
            {
                bodyList[i].isTrue = false;
            }
        }
    }
    public static void Reset(List<MessageBody> bodyList)
    {

        // Set all elements to false
        for (int i = 0; i < bodyList.Count; i++)
        {
            
                bodyList[i].isTrue = false;
        }
    }
    public static MessageBody GetMessageBody(List<MessageBody> bodyList, string identifier)
    {
        return bodyList.Where(x => x.Identifier == identifier).FirstOrDefault();
    }
    public static List<string> SetPhoneNumber(string phoneNumber){

        PhoneNumber.PhoneNumbers = new List<string>{phoneNumber};
        return PhoneNumber.PhoneNumbers;
    }
    public static List<string> GetPhoneNumbers(){
        return PhoneNumber.PhoneNumbers;
    }
}

public class MessageBody
{
    public string Identifier { get; set; }
    public string Message { get; set; }
    public bool isTrue { get; set; }
}
public static class  PhoneNumber{
 public static List<string> PhoneNumbers { get; set; }
 
};
