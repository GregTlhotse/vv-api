public static class Menu
{
    public static string TwoMountainsTitle = "Hello [name]! Welcome to Two Mountains. How can I assist you today? \ud83d\ude0a \n";
    public static string TwoMountainsMenu = "1️⃣ : How to claim.\n" +
                    "2️⃣ : Check claim status.\n" +
                    "3️⃣ : Update contact details.\n" +
                    "4️⃣ : Check policy status.\n" +
                    "5️⃣ : Check pickup status.\n" +
                    "6️⃣ : Create pick up.\n";
    public static string TypeMenu = "\n\n _Type *Menu* to go back to main menu_";
}
public static class BodyText
{
    public static string EnterClaimText = "Please provide me with your policy number so I can assist you further.";
    public static string EnterMissingContactText = "I'm sorry to hear that. It's possible there may be an issue with the contact details we have on file. To assist you better, can you please provide me with your ID number?";
    public static string EnterLocationText = "Let's start with your \uD83D\uDCCD pickup. You can either *manually enter an address* or *share your location.*.";
    public static string IMAGE = "https://www.twomountains.co.za/wp-content/uploads/2022/05/Two-Mountains-New-Logo.png";
    public static string EnterThankForPolicy = "Thank you for providing your policy number. Please wait a moment while I verify your claim status.";

    public static string EnterFoundClaimText = "I found your claim. It has been approved but not yet paid. I understand the importance of this, and I'll help you through the process. To ensure your payment is processed, I'll need to add it to our upcoming payment batch.\n\n\nMay I know if you have any specific details you'd like to be added to the batch?";
    public static string EnterStatusShowMissingDocumentsText = "The status shows that there are some missing documents causing the delay. You can easily submit the missing documents here. Would you like to proceed with that?";
    public static string EnterTypicallyRequireText = "Typically, we require basic information like your full name, contact number, and bank details for the transaction. However, if there's any additional or specific information you think we should know, please mention it.";
    public static string EnterUpcomingPaymentText = "Thank you for the information. I have added your details to our upcoming payment batch. Our finance team will process it, and I'll let you know once it's uploaded.\n\nCan you wait for a moment while I communicate with our finance team?";
    public static string EnterFinanceDepCom = "Thank you for your patience. I've communicated with our finance department, and your payment has been added to batch number [batchNumber]. You can expect your payment to be processed by [payDay].\n\nIf there's any delay or if you don't receive the payment by then, please contact our customer service at 0800 668 539.";
    public static string EnterNearestBranchText = "Choose your nearest branch please: ";
    public static string CreatedPickupText = "Your pickup has been created succesfully. Thank you \ud83d\ude0a";
    public static string EnterDeceasedNameText = "Please enter the *deceased first name*";
    public static string EnterDeceasedSurnameText = "Please enter the *deceased surname*";
    public static string EnterDeceasedPlaceOfDeathText = "Please enter the *deceased place of death*";

    public static string EnterThankYouText = "You're welcome! If you have any other questions or need further assistance in the future, please don't hesitate to reach out. Have a great day! \ud83d\ude0a";

    public static string EnterDeceasedThankYouText = "Thank you for the information provided \uD83D\uDE4F. ";
    public static string EnterSorryText = "Sorry something went wrong while receiving information provided.";
    public static string EnterGoAheadUpoladText = "Great! Please upload the necessary documents here.";
    public static string EnterYouNeedToUploadText = "You need to upload the necessary documents here.";
    public static string EnterThankYouSubmittedUploadText = "Thank you for submitting the documents. I have forwarded them for verification. Once they're verified, your claim process will continue. You will receive updates on the progress via SMS. Please ensure your contact details are up to date with us...";
    public static string EnterCashClaimText = "Your claim was recently submitted, and it generally takes up to 14 days to determine the result of a cash claim. You're still within that time frame. We appreciate your patience. You'll receive SMS updates as we move through the process.";
    public static string EnterFoundContactText = "I found the contact details linked to your policy. For security reasons, I can't display them here, but I can assist you in updating them if needed. However, before we proceed, we need to verify that I'm communicating with the correct policyholder.\n\nPlease provide me with your date of birth and the answer to your security question: [Security Question, e.g., What's your mother's maiden name?].";
    public static string EnterVerifyContact = "Thank you for verifying your identity. Please provide the correct contact number where you wish to receive the SMS updates.";
    public static string EnterConfirmedSecurityText = "Thank you. I have updated your contact number to [incomingText]. Moving forward, you should receive all SMS updates on this number.\n\nIf you face any issues or if there's any delay in receiving updates, please contact our customer service at 0800 668 539.";
    public static string EnterClaimNotFoundText = "I'm sorry, I'm unable to fetch the status of your claim at the moment. Please contact our customer service at 0800 668 539 for further assistance. My apologies for any inconvenience.";
    public static string EnterPolicyStatusText = "Hi [name]\n\n Your status is [status]\n\n Thank you.";
    public static string EnterPolicyStatusNotFoundText = "Your policy was not found! Please verify your policy number.\n\n\n9️⃣9️⃣) Exit";
    


}
