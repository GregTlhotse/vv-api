public static class PickupManager{
    public static List<BranchResponse> branchResponses = new List<BranchResponse>();
    public static BranchResponse branch = new BranchResponse();
    public static LocationResponse location = new LocationResponse();
    public static string TYPEADDRESS = String.Empty;
    public static string RECORDID = String.Empty;
    public static string FirstName = String.Empty;
    public static string Surname = String.Empty;
    public static string DeceasedPlaceOfDeath = String.Empty;
}