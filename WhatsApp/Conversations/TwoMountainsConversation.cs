using System.Text;
using ezra360.Proxy.Interfaces;

namespace ezra360.Proxy.WhatsAppService;

public class TwoMountainsConversation:ITwoMountainsConversation{
readonly IConfiguration _configuration;
        readonly IMessageAPIService _messageApi;
        readonly IEzraService _ezraApi;
        static List<MessageBody> messageBodies = new List<MessageBody>();

        public TwoMountainsConversation(IConfiguration configuration, IMessageAPIService api, IEzraService ezraApi, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _messageApi = api;
            _ezraApi = ezraApi;
            messageBodies = MessageManager.BodyList;
        }
        public async Task TwoMountainsConversationsAction(WhatsAppResponse response){
             // Initialise webhook responses 
            string name = response.entry[0].changes[0].value.contacts[0].profile.name;
            string senderPhoneNumber = response.entry[0].changes[0].value.messages != null ? response.entry[0].changes[0].value.messages[0].from : "";
            string senderId = response.entry[0].changes[0].value.metadata != null ? response.entry[0].changes[0].value.metadata.phone_number_id : "";
            string incomingText = (response.entry[0].changes[0].value.messages != null && response.entry[0].changes[0].value.messages[0].document == null && response.entry[0].changes[0].value.messages[0].interactive == null && response.entry[0].changes[0].value.messages[0].location == null && response.entry[0].changes[0].value.messages[0].button == null) ? response.entry[0].changes[0].value.messages[0].text.body : "";
            string message_id = response.entry[0].changes[0].value.messages != null ? response.entry[0].changes[0].value.messages[0].id : "";
            string document = response.entry[0].changes[0].value.messages[0].document != null ? response.entry[0].changes[0].value.messages[0].document.id : "";
            incomingText = (incomingText == "" && response.entry[0].changes[0].value.messages[0].interactive != null && response.entry[0].changes[0].value.messages[0].document == null && response.entry[0].changes[0].value.messages[0].location == null) ? response.entry[0].changes[0].value.messages[0].interactive.button_reply.title : incomingText;
            if (response.entry[0].changes[0].value.messages[0].location != null)
            {
                PickupManager.location = response.entry[0].changes[0].value.messages[0].location;
            }
            else if (MessageManager.GetMessageBody(messageBodies, "EnterBranchesTitleText").isTrue == true)
            {
                PickupManager.TYPEADDRESS = incomingText;
            }

            //Template menu
            if (response.entry[0].changes[0].value.messages[0].button != null)
            {
                incomingText = response.entry[0].changes[0].value.messages[0].button.text;
            }

            string twoMountains = Menu.TwoMountainsTitle.Replace("[name]", name);
            string twoMountainsMenu = Menu.TwoMountainsMenu;
            string menu = Menu.TypeMenu;

            var sendWhatsApp = new SendWhatsApp
            {
                messaging_product = "whatsapp",
                to = senderPhoneNumber,
                type = "text",
                recipient_type = "individual",
                text = new SendText { preview_url = true, body = twoMountains + twoMountainsMenu }

            };


            switch (incomingText)
            {
                case "How to claim" when (MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").isTrue == false && MessageManager.GetMessageBody(messageBodies, "EnterDeceasedNameText").isTrue == false):
                    BooleanManager.isHowToClaim = true;
                    sendWhatsApp.text = new SendText { preview_url = true, body = "https://www.twomountains.co.za/" + menu };
                    await _messageApi.MarkAsRead(message_id, senderId);
                    await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                    break;
                case "Check claim status" when (MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").isTrue == false && MessageManager.GetMessageBody(messageBodies, "EnterDeceasedNameText").isTrue == false):
                    BooleanManager.isClaim = true;
                    sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterClaimText").Message + menu };
                    await _messageApi.MarkAsRead(message_id, senderId);
                    await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                    MessageManager.SetTrueAtIndex(messageBodies, "EnterThankForPolicy");
                    break;
                case "Update contact details" when (MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").isTrue == false && MessageManager.GetMessageBody(messageBodies, "EnterDeceasedNameText").isTrue == false):
                    BooleanManager.isMissingContact = true;
                    sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterMissingContactText").Message + menu };
                    await _messageApi.MarkAsRead(message_id, senderId);
                    await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                    MessageManager.SetTrueAtIndex(messageBodies, "EnterFoundContactText");
                    break;
                case "Check policy status" when (MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").isTrue == false && MessageManager.GetMessageBody(messageBodies, "EnterDeceasedNameText").isTrue == false):
                    BooleanManager.isPolicyStatus = true;//
                    sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterClaimText").Message + menu };
                    await _messageApi.MarkAsRead(message_id, senderId);
                    await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                    MessageManager.SetTrueAtIndex(messageBodies, "EnterPolicyStatusText");
                    break;
                case "Create pick up" when (MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").isTrue == false && MessageManager.GetMessageBody(messageBodies, "EnterDeceasedNameText").isTrue == false):
                    BooleanManager.isPickup = true;
                    var location = new Location
                    {
                        messaging_product = "whatsapp",
                        to = senderPhoneNumber,
                        type = "interactive",
                        recipient_type = "individual",
                        interactive = new LocationInteractive
                        {
                            type = "location_request_message",
                            body = new LocationBody { text = MessageManager.GetMessageBody(messageBodies, "EnterLocationText").Message + menu },
                            action = new LocationAction { name = "send_location" }
                        }
                    };
                    await _messageApi.MarkAsRead(message_id, senderId);
                    await _messageApi.SendLocationWhatsApp(location, senderId);
                    MessageManager.GetMessageBody(messageBodies, "EnterBranchesTitleText").isTrue = true;
                    break;
                case "Menu":
                    BooleanManager.Reset();
                    MessageManager.Reset(messageBodies);
                    sendWhatsApp.type = "image";
                    sendWhatsApp.image = new Image { link = MessageManager.GetMessageBody(messageBodies, "IMAGE").Message, caption = twoMountains + twoMountainsMenu };
                    await _messageApi.MarkAsRead(message_id, senderId);
                   // await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                   var menuTemplate = new MenuTemplate{
                            messaging_product = "whatsapp",
                            recipient_type = "individual",
                            to = senderPhoneNumber,
                            type =  "template",
                            template = new Template {
                                name =  "menu",
                                language = new Language {
                                    code = "en_US"
                                }
                            }   
                        };
                        await _messageApi.SendMenuTemplateWhatsApp(menuTemplate,senderId);
                    break;

                default:
                    // Scenerio 1
                    if (MessageManager.GetMessageBody(messageBodies, "EnterThankForPolicy").isTrue == true)
                    {

                        BooleanManager.policyNumber = incomingText;
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterThankForPolicy").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        //Check details on Ezra then send again

                        //It has been approved but not yet paid or It's currently pending
                        System.Threading.Thread.Sleep(10000);
                        var isFound = false;
                        if (isFound == true)
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterFoundClaimText").Message + menu };
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            MessageManager.SetTrueAtIndex(MessageManager.BodyList, "EnterFoundClaimText");
                            BooleanManager.lookForDetails = true;
                            BooleanManager.isClaim = false;
                            MessageManager.GetMessageBody(messageBodies, "EnterThankForPolicy").isTrue = false;
                        }
                        else
                        {
                            InteractiveButtonViewModel interactiveButtonViewModel = new InteractiveButtonViewModel
                            {
                                Button_Title = "Choose...",
                                Button_1 = "Yes",
                                Button_2 = "No",
                                SenderPhoneNumber = sendWhatsApp.to
                            };
                            sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterStatusShowMissingDocumentsText").Message + menu };
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            await WhatsAppInteractiveButtons(interactiveButtonViewModel);
                            BooleanManager.isPendingDocument = true;
                            BooleanManager.isClaim = false;
                            MessageManager.GetMessageBody(messageBodies, "EnterThankForPolicy").isTrue = false;
                            BooleanManager.isEarlyDays = false;//EnterCashClaimText
                            MessageManager.GetMessageBody(messageBodies, "EnterCashClaimText").isTrue = false;
                            MessageManager.SetTrueAtIndex(messageBodies, "EnterStatusShowMissingDocumentsText");
                        }

                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterFoundClaimText").isTrue == true)
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterTypicallyRequireText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.lookForDetails = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterFoundClaimText").isTrue = false;
                        BooleanManager.foundDetails = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterUpcomingPaymentText");
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterUpcomingPaymentText").isTrue == true)
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterUpcomingPaymentText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.foundDetails = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterUpcomingPaymentText").isTrue = false;
                        BooleanManager.communicateFinance = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterFinanceDepCom");
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterFinanceDepCom").isTrue == true)
                    {
                        var payDay = DateTime.Now.AddDays(1);
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterFinanceDepCom").Message.Replace("[batchNumber]", BooleanManager.policyNumber).Replace("[payDay]", payDay.ToString("dd/MM/yyyy")) + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.communicateFinance = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterFinanceDepCom").isTrue = false;
                        break;
                    }
                    // ....... 1 end

                    //Scenario 2
                    if (MessageManager.GetMessageBody(messageBodies, "EnterStatusShowMissingDocumentsText").isTrue == true && (incomingText.ToLower() == "yes" || incomingText.ToLower() == "yea" || incomingText.ToLower() == "yeah" || incomingText.ToLower() == "ja"))
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterGoAheadUpoladText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isPendingDocument = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterStatusShowMissingDocumentsText").isTrue = false;
                        BooleanManager.isDocumentUploaded = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterThankYouSubmittedUploadText");
                        break;
                    }
                    else if (MessageManager.GetMessageBody(messageBodies, "EnterStatusShowMissingDocumentsText").isTrue == true && incomingText.ToLower() == "no" || incomingText.ToLower() == "nope")
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterYouNeedToUploadText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isPendingDocument = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterStatusShowMissingDocumentsText").isTrue = false;
                        BooleanManager.isDocumentUploaded = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterThankYouSubmittedUploadText");
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterThankYouSubmittedUploadText").isTrue == true && String.IsNullOrEmpty(incomingText))
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterThankYouSubmittedUploadText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        await _messageApi.GetDocument(document);
                        BooleanManager.isDocumentUploaded = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterThankYouSubmittedUploadText").isTrue = false;
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterCashClaimText").isTrue == true)
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterCashClaimText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isEarlyDays = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterCashClaimText").isTrue = false;
                        break;
                    }
                    // ....... 2 end

                    //Scenario 3
                    if (MessageManager.GetMessageBody(messageBodies, "EnterFoundContactText").isTrue == true)
                    {
                        // Check if the contact exists
                        var isContactFound = true;
                        if (isContactFound == true)
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterFoundContactText").Message + menu };
                            await _messageApi.MarkAsRead(message_id, senderId);
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            BooleanManager.isCorrectContactSecurity = true;
                            MessageManager.SetTrueAtIndex(messageBodies, "EnterVerifyContact");
                        }
                        BooleanManager.isMissingContact = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterFoundContactText").isTrue = false;
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterVerifyContact").isTrue == true)
                    {
                        //Check Security
                        var isCorrectSecurity = true;
                        if (isCorrectSecurity == true)
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterVerifyContact").Message + menu };
                            await _messageApi.MarkAsRead(message_id, senderId);
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            BooleanManager.isConfirmedContactSecurity = true;
                            MessageManager.SetTrueAtIndex(messageBodies, "EnterConfirmedSecurityText");
                        }
                        BooleanManager.isCorrectContactSecurity = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterVerifyContact").isTrue = false;
                        break;

                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterConfirmedSecurityText").isTrue == true)
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterConfirmedSecurityText").Message.Replace("[incomingText]", incomingText) + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isConfirmedContactSecurity = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterConfirmedSecurityText").isTrue = false;
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterClaimNotFoundText").isTrue == true)
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterClaimNotFoundText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.claimNotFound = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterClaimNotFoundText").isTrue = false;
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterPolicyStatusText").isTrue == true)
                    {
                        var ezra = await _ezraApi.GetPolicyStatus(incomingText);
                        if (ezra.Count > 0)
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterPolicyStatusText").Message.Replace("[name]", ezra[0].ClientIdName).Replace("[status]", ezra[0].Status) + menu };
                            await _messageApi.MarkAsRead(message_id, senderId);
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            BooleanManager.isPolicyStatus = false;
                            MessageManager.GetMessageBody(messageBodies, "EnterPolicyStatusText").isTrue = false;
                        }
                        else
                        {
                            if (incomingText != "99")
                            {
                                sendWhatsApp.text = new SendText { preview_url = true, body = BodyText.EnterPolicyStatusNotFoundText };
                                await _messageApi.MarkAsRead(message_id, senderId);
                                await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                                MessageManager.SetTrueAtIndex(messageBodies, "EnterPolicyStatusText");
                            }
                        }
                        if (incomingText == "99")
                        {
                            MessageManager.GetMessageBody(messageBodies, "EnterPolicyStatusText").isTrue = false;
                            BooleanManager.isPolicyStatus = false;
                            incomingText = "hello";
                        }
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterBranchesTitleText").isTrue == true)
                    {
                        var sb = new StringBuilder();//EnterBranchesTitleText
                        var title = MessageManager.GetMessageBody(messageBodies, "EnterBranchesTitleText").Message;
                        PickupManager.branchResponses = await _ezraApi.GetBranches();
                        for (var i = 0; i < PickupManager.branchResponses.Count; i++)
                        {

                            sb.Append(i + 1 + ")\t" + PickupManager.branchResponses[i].Name);
                            sb.AppendLine();
                        }

                        sendWhatsApp.text = new SendText { preview_url = true, body = title + sb.ToString() + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isPickingBranch = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterPlaceOfPickupTitleText");
                        BooleanManager.isPickup = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterBranchesTitleText").isTrue = false;
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").isTrue == true)
                    {
                        var title = MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").Message;
                        PickupManager.branch = PickupManager.branchResponses[Convert.ToInt32(incomingText) - 1];
                        sendWhatsApp.text = new SendText { preview_url = true, body = title + "1) Home" + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isPickingBranch = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterPlaceOfPickupTitleText").isTrue = false;
                        BooleanManager.isPickingPickup = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterDeceasedNameText");
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterDeceasedNameText").isTrue == true)
                    {
                        var data = new Pickup
                        {
                            entityName = "PickUp",
                            requestName = "UpsertRecordReq",
                            inputParamters = new InputParamters
                            {
                                Entity = new Entity
                                {
                                    SourceChannel = "887",
                                    PickUpStatus = "894",
                                    PlaceOfPickUp = "900",
                                    PickUpAddress = String.Empty,
                                    Branch = PickupManager.branch.BranchId,
                                    DueDate = DateTime.Now.AddDays(1).ToString().Replace("/", "-"),
                                    Informant1FullName = name,
                                    Informant1Contact = senderPhoneNumber
                                }
                            }
                        };
                        if (PickupManager.location.longitude > 0)
                        {
                            data.inputParamters.Entity.PickUpAddress = PickupManager.location.address != null ? PickupManager.location.address : "";
                            data.inputParamters.Entity.Latitude = PickupManager.location.latitude.ToString();
                            data.inputParamters.Entity.Longitude = PickupManager.location.longitude.ToString();
                        }
                        else
                        {
                            data.inputParamters.Entity.PickUpAddress = PickupManager.TYPEADDRESS;
                        }

                        var result = await _ezraApi.CreatePickup(data);
                        if (result.isSuccess)
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = BodyText.EnterDeceasedNameText + menu };
                            await _messageApi.MarkAsRead(message_id, senderId);
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            BooleanManager.isPickingPickup = false;
                            MessageManager.GetMessageBody(messageBodies, "EnterDeceasedNameText").isTrue = false;
                            PickupManager.TYPEADDRESS = String.Empty;
                            PickupManager.location = new LocationResponse();
                            PickupManager.RECORDID = result.outputParameters.UpsertResponse.recordId;
                            BooleanManager.isDeceasedFirstName = true;
                            MessageManager.SetTrueAtIndex(messageBodies, "EnterDeceasedSurnameText");
                            break;
                        }
                        else
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterSorryText").Message + menu };
                            await _messageApi.MarkAsRead(message_id, senderId);
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            break;
                        }
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterDeceasedSurnameText").isTrue == true)
                    {
                        PickupManager.FirstName = incomingText;
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterDeceasedSurnameText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isDeceasedFirstName = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterDeceasedSurnameText").isTrue = false;
                        BooleanManager.isDeceasedSurname = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterDeceasedPlaceOfDeathText");
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterDeceasedPlaceOfDeathText").isTrue == true)
                    {
                        PickupManager.Surname = incomingText;
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterDeceasedPlaceOfDeathText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        BooleanManager.isDeceasedSurname = false;
                        MessageManager.GetMessageBody(messageBodies, "EnterDeceasedPlaceOfDeathText").isTrue = false;
                        BooleanManager.isDeceasedDone = true;
                        MessageManager.SetTrueAtIndex(messageBodies, "EnterDeceasedThankYouText");
                        break;
                    }
                    if (MessageManager.GetMessageBody(messageBodies, "EnterDeceasedThankYouText").isTrue == true)
                    {
                        PickupManager.DeceasedPlaceOfDeath = incomingText;
                        var data = new Deseased
                        {
                            entityName = "Deceased",
                            requestName = "UpsertRecordReq",
                            inputParamters = new DeseasedInputParamters
                            {
                                Entity = new DeseasedEntity
                                {
                                    FirstName = PickupManager.FirstName,
                                    Surname = PickupManager.Surname,
                                    PlaceOfDeath = PickupManager.DeceasedPlaceOfDeath,
                                    PickUpId = PickupManager.RECORDID
                                }
                            }
                        };
                        var result = await _ezraApi.CreateDeseased(data);
                        if (result.isSuccess)
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterDeceasedThankYouText").Message + MessageManager.GetMessageBody(messageBodies, "CreatedPickupText").Message + menu };
                            await _messageApi.MarkAsRead(message_id, senderId);
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            BooleanManager.isDeceasedDone = false;
                            MessageManager.GetMessageBody(messageBodies, "EnterDeceasedThankYouText").isTrue = false;
                            PickupManager.FirstName = String.Empty;
                            PickupManager.DeceasedPlaceOfDeath = String.Empty;
                            PickupManager.Surname = String.Empty;
                            PickupManager.RECORDID = String.Empty;
                            break;
                        }
                        else
                        {
                            sendWhatsApp.text = new SendText { preview_url = true, body = BodyText.EnterSorryText + menu };
                            await _messageApi.MarkAsRead(message_id, senderId);
                            await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                            BooleanManager.isDeceasedDone = false;
                            MessageManager.GetMessageBody(messageBodies, "EnterDeceasedThankYouText").isTrue = false;
                            PickupManager.FirstName = String.Empty;
                            PickupManager.DeceasedPlaceOfDeath = String.Empty;
                            PickupManager.Surname = String.Empty;
                            PickupManager.RECORDID = String.Empty;
                            break;
                        }
                    }

                    if (incomingText.ToLower() == "thank you for your help" || incomingText.ToLower() == "thank you for the information" || incomingText.ToLower() == "thank you" || incomingText.ToLower() == "thanks" || incomingText.ToLower() == "dankie")
                    {
                        sendWhatsApp.text = new SendText { preview_url = true, body = MessageManager.GetMessageBody(messageBodies, "EnterThankYouText").Message + menu };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        break;
                    }

                    if (incomingText.ToLower() == "hi" || incomingText.ToLower() == "hello" || incomingText.ToLower() == "sho"|| incomingText.ToLower() == "hey"  || incomingText.ToLower() == "hy" || incomingText.ToLower() == "xo" || incomingText.ToLower() == "eita" || incomingText.ToLower() == "ekse" || incomingText.ToLower() == "hi there" || incomingText.ToLower() == "hey there" || incomingText.ToLower() == "hello there")
                    {
                        sendWhatsApp.type = "image";
                        sendWhatsApp.image = new Image { link = MessageManager.GetMessageBody(messageBodies, "IMAGE").Message, caption = twoMountains };
                        await _messageApi.MarkAsRead(message_id, senderId);
                        await _messageApi.SendWhatsApp(sendWhatsApp, senderId);
                        System.Threading.Thread.Sleep(5000);
                        var menuTemp = new MenuTemplate{
                            messaging_product = "whatsapp",
                            recipient_type = "individual",
                            to = senderPhoneNumber,
                            type =  "template",
                            template = new Template {
                                name =  "menu",
                                language = new Language {
                                    code = "en_US"
                                }
                            }   
                        };
                        await _messageApi.SendMenuTemplateWhatsApp(menuTemp,senderId);


                        BooleanManager.isWelcome = false;
                        break;
                    }
                    break;
            }
            
        }
public async Task WhatsAppReply(ReplyViewModel replyViewModel)
        {
            var sendWhatsApp = new SendWhatsApp
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "text",
                recipient_type = "individual",
                text = new SendText { preview_url = true, body = replyViewModel.IncomingText }

            };
            await _messageApi.SendWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppReplyMessage(ReplyMessageViewModel replyMessageViewModel)
        {
            var sendReplyWhatsApp = new ReplyWhatsApp
            {
                messaging_product = "whatsapp",
                to = replyMessageViewModel.SenderPhoneNumber,
                type = "text",
                recipient_type = "individual",
                text = new ReplyText { preview_url = true, body = replyMessageViewModel.IncomingText },
                context = new Context { message_id = replyMessageViewModel.MessageId }

            };
            await _messageApi.SendMessageReplyWhatsApp(sendReplyWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppWelcomeWithImage(ReplyViewModel replyViewModel)
        {
            var sendWhatsApp = new SendWhatsApp
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "image",
                recipient_type = "individual",
                image = new Image { link = BodyText.IMAGE, caption = replyViewModel.IncomingText }

            };
            await _messageApi.SendWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppInteractiveButtons(InteractiveButtonViewModel replyViewModel)
        {
            var btn1 = new Button
            {
                type = "reply",
                reply = new Reply { id = "1", title = replyViewModel.Button_1 }
            };
            var btn2 = new Button
            {
                type = "reply",
                reply = new Reply { id = "2", title = replyViewModel.Button_2 }
            };
            var list = new List<Button>
            {
                btn1,
                btn2
            };

            var sendWhatsApp = new InteractiveButton
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "interactive",
                recipient_type = "individual",
                interactive = new InteractiveBtn { type = "button", body = new Body { text = replyViewModel.Button_Title }, action = new Action { buttons = list } }

            };
            await _messageApi.SendinteractiveWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
        public async Task WhatsAppBranchButtons(InteractiveButtonViewModel replyViewModel)
        {
            var btn1 = new Button
            {
                type = "reply",
                reply = new Reply { id = "1", title = replyViewModel.Button_1 }
            };
            var btn2 = new Button
            {
                type = "reply",
                reply = new Reply { id = "2", title = replyViewModel.Button_2 }
            };
            var list = new List<Button>
            {
                btn1,
                btn2
            };

            var sendWhatsApp = new InteractiveButton
            {
                messaging_product = "whatsapp",
                to = replyViewModel.SenderPhoneNumber,
                type = "interactive",
                recipient_type = "individual",
                interactive = new InteractiveBtn { type = "button", body = new Body { text = replyViewModel.Button_Title }, action = new Action { buttons = list } }

            };
            await _messageApi.SendinteractiveWhatsApp(sendWhatsApp, _configuration.GetValue<string>("WhatsAppSettings:TwoMountainsSenderId"));
        }
}