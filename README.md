# whatsapp API

How to set up WhatsApp Bot for .net developers

1. Create Facebook app (https://developers.facebook.com/docs/development/create-an-app/) - link your business profile with this app
2. Set up WhatsApp Business API
3. Create permenant token(documentation available on the site -> https://developers.facebook.com/docs/whatsapp/business-management-api/get-started#1--acquire-an-access-token-using-a-system-user-or-facebook-login) or use temporary token(To use on the graph API)
4. Set up WhatsApp Webhook(An end point from your server and your defined token -> any wording of your choose)
5. Set up testing phone number
6. Then test

You need to write a code to have an endpoint that will be used to register your webhook.

Then deploy the code on a server with this endpoint as an example: https://www.domain.com/webhook.

C# code:
```
[Route("webhook")]
[HttpPost, HttpGet]
public async Task<IActionResult> PostAsyc()
{
    var req = Request;
			try
            {
                string mode = req.Query["hub.mode"];
                string challenge = req.Query["hub.challenge"];
                string verifyToken = req.Query["hub.verify_token"];
                if (!String.IsNullOrEmpty(mode) && !String.IsNullOrEmpty(challenge))
                {
                    if (mode == "subscribe" && verifyToken == "Hello Greg") //Hello Greg is my "Verify Token" on the WhatsApp configuration
                    {
                        string responseMessage = challenge;
                        return new OkObjectResult(responseMessage);
                    }
                    else
                    {
                        return new BadRequestObjectResult("Error");
                    }
                }
                else
                {
		  // subscribe and send whatsApp messages(Check graph API-> messages)
                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }			
}
```
End
